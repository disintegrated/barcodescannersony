package de.tum.mw.fml.barcodescannersony;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.zxing.Result;
import com.sony.smarteyeglass.extension.util.SmartEyeglassControlUtils;
import com.sonyericsson.extras.liveware.aef.control.Control;
import com.sonyericsson.extras.liveware.extension.util.ExtensionUtils;
import com.sonyericsson.extras.liveware.extension.util.control.ControlExtension;

import com.sony.smarteyeglass.extension.util.CameraEvent;
import com.sony.smarteyeglass.extension.util.SmartEyeglassEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RabbidDog on 12/20/2015.
 */
public class CaptureControl extends ControlExtension {
    /**
     * Create control extension.
     *
     * @param context            The extension service context.
     * @param hostAppPackageName Package name of host application.
     */

    /** The application context. */
    private final String TAG = "Capture Control";
    private final Context context;
    private final SmartEyeglassControlUtils mSmartEyeglassControlUtils;
    private static final int SMARTEYEGLASS_API_VERSION = 3;
    /** The camera mode. */
    private AbstractCameraMode mode;
    private long startTime;
    private long endTime;
    private SharedPreferences mSharedPreferences;
    private int mCameraZoom;
    private IntentFilter mIntentFilter;
    private BroadcastReceiver mBroadcastReceiver;
    private boolean scanInProgress;


    public CaptureControl(final Context context,final String hostAppPackageName)
    {
        super(context, hostAppPackageName);
        Log.d(TAG, "CaptureControl: constructor");
        this.context = context;
        scanInProgress = false;
        mSharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        SmartEyeglassEventListener listener = new SmartEyeglassEventListener(){
            @Override
            public void onCameraReceived(final CameraEvent event)
            {
                Log.d(TAG, "onCameraReceived: ");
                mode.handleCameraEvent(event);
            }
            // Called when camera operation has failed
            // We just log the error
            @Override
            public void onCameraErrorReceived(final int error) {
                Log.d(Constants.LOG_TAG, "onCameraErrorReceived: " + error);
            }
            // When camera is set to record image to a file,
            // log the operation
            @Override
            public void onCameraReceivedFile(final String filePath) {
                Log.d(Constants.LOG_TAG, "onCameraReceivedFile: " + filePath);
                //mode.closeCamera(utils);
            }
        };
        setCameraMode();
        mSmartEyeglassControlUtils = new SmartEyeglassControlUtils(hostAppPackageName, listener);
        mSmartEyeglassControlUtils.setRequiredApiVersion(SMARTEYEGLASS_API_VERSION);
        mSmartEyeglassControlUtils.activate(context);

        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(context.getString( R.string.set_result_intent));

        mBroadcastReceiver = new BroadcastReceiver() {
            /** Receives the broadcast that has been fired */
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction()== context.getString(R.string.set_result_intent)){
                    Log.d(TAG, "onReceive: set_result broadcast received");
                    //HERE YOU WILL GET VALUES FROM BROADCAST THROUGH INTENT EDIT YOUR TEXTVIEW///////////
                    String resultValue = intent.getStringExtra("result");
                    Log.d(TAG, "onReceive: calling outer class showDecodeResult");
                    CaptureControl.this.showDecodeResult(resultValue);
                }
            }

        };
    }

    // Clean up data structures on termination.
    @Override
    public void onDestroy() {
        scanInProgress = false;
        mSmartEyeglassControlUtils.deactivate();
    };

    // When app becomes visible, set up camera mode choices
    // and instruct user to begin camera operation
    @Override
    public void onResume() {
        Log.d(TAG, "onResume: start");
        /*register receiver*/
        mContext.registerReceiver(mBroadcastReceiver, mIntentFilter);
        Log.d(TAG, "onResume: broadcast receiver registered");
        // Keep the screen on for this demonstration.
        // Don't do this in a real app, it will drain the battery.
        setScreenState(Control.Intents.SCREEN_STATE_AUTO);
        setCameraMode();
        // Get and show quality parameters
        int jpegQuality = Integer.parseInt(mSharedPreferences.getString(
                context.getString(R.string.preference_key_jpeg_quality), "1"));
        int preferenceId = mode.getPreferenceId();
        int resolution = Integer.parseInt(mSharedPreferences.getString(
                context.getString(preferenceId), "6"));
        mode.mZoom = Integer.parseInt(mSharedPreferences.getString(context.getString(R.string.preference_key_zoom), "2"));
        // Set the camera mode to match the setup
        mSmartEyeglassControlUtils.setCameraMode(jpegQuality, resolution, mode.getRecordingMode());
        Log.d(TAG, "onResume: cameramode set");

        showLayout(R.layout.scanner_main, null); // setting to null for debugging
        Log.d(TAG, "onResume: end");
    }

    // Clean up any open files and reset mode when app is paused.
    @Override
    public void onPause() {
        Log.d(TAG, "onPause: start");
        /*deregister receiver*/
        mContext.unregisterReceiver(mBroadcastReceiver);

        Log.d(TAG, "onPause: broadcast receiver unregistered");
        mode.closeCamera(mSmartEyeglassControlUtils);
        mode.onPause();
        mode = null;
        Log.d(TAG, "onPause: end");

        scanInProgress = false;
    }

    // Respond to tap on touch pad by switching camera modes.
    @Override
    public void onTap(final int action, final long timeStamp) {
        if (action != Control.TapActions.SINGLE_TAP) {
            return;
        }
        if(scanInProgress)
        {
            return;
        }
        startTime = System.currentTimeMillis();

        scanInProgress = true;
        sendText(R.id.main_message, "");
        /*setup decoder*/
        mode.toggleState(mSmartEyeglassControlUtils);
    }

    public void showDecodeResult(String resultValue)
    {
        mode.closeCamera(mSmartEyeglassControlUtils);
        scanInProgress = false;
        endTime = System.currentTimeMillis();
        Log.i(TAG, "showDecodeResult: endtime : " + endTime);
        //stop the decoder thread
        //decodeRunnable.stop();
        List<Bundle> data = new ArrayList<Bundle>();
        double duration = (endTime - startTime)/1000.0;

        Bundle bStatus = new Bundle();
        bStatus.putInt(Control.Intents.EXTRA_LAYOUT_REFERENCE, R.id.scan_status);
        Bundle bResult = new Bundle();
        bResult.putInt(Control.Intents.EXTRA_LAYOUT_REFERENCE, R.id.scan_result);
        Bundle bDuration = new Bundle();
        bDuration.putInt(Control.Intents.EXTRA_LAYOUT_REFERENCE, R.id.scan_duration);
        if(null != resultValue)
        {
            Log.i(TAG, "showDecodeResult: scan result was not null");
            Log.i(TAG, "showDecodeResult: "+resultValue);
            bStatus.putString(Control.Intents.EXTRA_TEXT, context.getString(R.string.scan_succeeded));

            bResult.putString(Control.Intents.EXTRA_TEXT, resultValue);
        }
        else
        {
            Log.i(TAG, "showDecodeResult: scan result was null");
            bStatus.putString(Control.Intents.EXTRA_TEXT, context.getString(R.string.scan_failed));

            bResult.putString(Control.Intents.EXTRA_TEXT, "NO RESULT");
        }

        bDuration.putString(Control.Intents.EXTRA_TEXT, duration + "Sec ");
        data.add(bStatus);
        data.add(bResult);
        data.add(bDuration);

        showLayout(R.layout.layout, data.toArray(new Bundle[data.size()]));
        mSmartEyeglassControlUtils.sendTextViewLayoutId(R.id.scan_result);
    }

    private void setCameraMode()
    {
        Log.d(TAG, "setCameraMode: ");
        // Track current camera mode parameters
        CameraModeFactory[] factories = CameraModeFactory.values();
        /*reload preferences*/
        mSharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        int recordMode = Integer.parseInt(mSharedPreferences.getString(
                context.getString(R.string.preference_key_recordmode), "0"));
        if (recordMode < 0 || recordMode >= factories.length) {
            recordMode = 0;
        }
        // Get and show current recording mode
        mode = factories[recordMode].of(context, this);
        Log.d(TAG, "setCameraMode: end");
    }

}
