package de.tum.mw.fml.barcodescannersony;

/**
 * Created by RabbidDog on 12/20/2015.
 */
import android.util.Log;

import com.sonyericsson.extras.liveware.extension.util.ExtensionService;
import com.sonyericsson.extras.liveware.extension.util.control.ControlExtension;
import com.sonyericsson.extras.liveware.extension.util.registration.DeviceInfoHelper;
import com.sonyericsson.extras.liveware.extension.util.registration.RegistrationInformation;
import com.sonyericsson.extras.liveware.extension.util.widget.WidgetExtension;

public class ScannerExtensionService  extends ExtensionService{

    public ScannerExtensionService()
    {
        super(Constants.EXTENSION_KEY);
    }
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(Constants.LOG_TAG, "ScannerExtensionService : onCreate");
    }

    @Override
    protected RegistrationInformation getRegistrationInformation() {
        Log.d(Constants.LOG_TAG, "ScannerExtensionService : getRegistrationInformation");
        return new ScannerRegistrationInformation(this);
    }

    @Override
    protected boolean keepRunningWhenConnected() {
        return false;
    }

    @Override
    public ControlExtension createControlExtension(
            final String hostAppPackageName) {
        Log.d(Constants.LOG_TAG, "ScannerExtensionService : createControlExtension");
        boolean isApiSupported = DeviceInfoHelper
                .isSmartEyeglassScreenSupported(this, hostAppPackageName);
        if (isApiSupported) {
            Log.d(Constants.LOG_TAG, "ScannerExtensionService : ApiSupported");
            return new CaptureControl(this, hostAppPackageName);
        } else {
            Log.d(Constants.LOG_TAG, "Service: not supported, exiting");
            throw new IllegalArgumentException(
                    "No control for: " + hostAppPackageName);
        }
    }

}
