package de.tum.mw.fml.barcodescannersony;

import android.util.Log;
import com.sony.smarteyeglass.extension.util.SmartEyeglassControlUtils;

/**
 * Created by RabbidDog on 1/14/2016.
 */
public class CaptureRunnable implements Runnable {

    private volatile Thread mCaptureRunnable;
    private SmartEyeglassControlUtils mUtils;
    private int captureCount;
    private String TAG = "CaptureRunnable";

    public CaptureRunnable(SmartEyeglassControlUtils utils)
    {
        mUtils = utils;
        captureCount = 0;
    }
    @Override
    public void run() {
        Log.d(TAG, "run: capture count"+(++captureCount));
        mCaptureRunnable = Thread.currentThread();
        while(null != mCaptureRunnable )
        {
            try
            {
                Log.d(TAG, "run: while loop to capture image");
                captureImage();
            }catch (InterruptedException ie)
            {
                Log.e(TAG, "run: interrupted exception", ie);
            }

        }
    }

    public synchronized  void captureImage() throws InterruptedException
    {
        Log.d(TAG, "captureImage: ");
        mUtils.requestCameraCapture();
        wait();
    }

    public synchronized void resumeCaptureImage()
    {
        Log.d(TAG, "resumeCaptureImage: ");
            notify();
    }

     public void stop()
     {
         /*volatile member checked in RUN to quit the thread*/
         mCaptureRunnable = null;
     }
}
