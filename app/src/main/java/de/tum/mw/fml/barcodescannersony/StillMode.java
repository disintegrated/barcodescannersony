/*
Copyright (c) 2011, Sony Mobile Communications Inc.
Copyright (c) 2014, Sony Corporation

 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 * Neither the name of the Sony Mobile Communications Inc.
 nor the names of its contributors may be used to endorse or promote
 products derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package de.tum.mw.fml.barcodescannersony;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.content.Context;

import com.sony.smarteyeglass.SmartEyeglassControl.Intents;
import com.sony.smarteyeglass.extension.util.ControlCameraException;
import com.sony.smarteyeglass.extension.util.SmartEyeglassControlUtils;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;

/**
 * The implementation of Still mode.
 */
public final class StillMode extends AbstractStillMode {


    /**
     * Creates a new instance.
     *
     * @param context
     *            The application context.
     */
    public StillMode(final Context context, final CaptureControl control) {
        super(context, control);
    }

    @Override
    public int getRecordingMode() {
        return Intents.CAMERA_MODE_STILL;
    }

    @Override
    protected void handlePictureData( final byte[] data) {
       /* Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
       // d.displayBitmap(bitmap); //may not require to display the bitmap
        byte[] copyBytes = Arrays.copyOf(data, data.length);
        Log.d(TAG, "handlePictureData: got picture data");

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Log.i(TAG, "handlePictureData: width:"+width+" height: "+height);*/
        callbackObj.onStillModeFrame(data);
    }

    @Override
    protected void willOpenCamera(final SmartEyeglassControlUtils utils)
            throws ControlCameraException {
        Log.d(TAG, "startCamera ");
        utils.startCamera();
        /*whenever camera opens then new latch is created*/
        frameInitLatch = new CountDownLatch(1);
        nullIfResultSetLock = new Object();
    }

    @Override
    public void setCallback(IStillModeCallback obj)
    {
        this.callbackObj = obj;
        frameInitLatch.countDown();
    }

    @Override
    protected void privateUtility(final SmartEyeglassControlUtils utils)
    {
        /*the private utility of this child class is to use the decoderunner for scanning images*/
        /*wait for call back to be set*/

        /*if call back is set to null, stop*/
        /*while((null != callbackObj) || (null != nullIfResultSetLock))
        {
            utils.requestCameraCapture();
        }*/
        /*if result was set then stop decoderunnable thread and return result*/
        /*if(null == nullIfResultSetLock)
        {
            decodeRunnable.stop();
            mControl.setResult(result);
        }*/
    }
}
