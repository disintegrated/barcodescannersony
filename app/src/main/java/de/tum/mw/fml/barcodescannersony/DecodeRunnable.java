package de.tum.mw.fml.barcodescannersony;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * Created by RabbidDog on 11/10/2015.
 */
/*final class DecodeRunnable implements Runnable, Camera.PreviewCallback
{
    private static final String TAG = DecodeRunnable.class.getSimpleName();
    private final CaptureActivity activity;
    private final Camera camera;
    private Handler handler;
    private final CountDownLatch handlerInitLatch;
    private boolean running;
    private byte[] previewBuffer;
    private final int height;
    private final int width;

    DecodeRunnable(CaptureActivity captureActivity, Camera camera)
    {
        this.activity = captureActivity;
        this.camera = camera;
        running = true;
        handlerInitLatch = new CountDownLatch(1);
        Camera.Parameters parameters = camera.getParameters();
        Camera.Size previewSize = parameters.getPreviewSize();
        height = previewSize.height;
        width = previewSize.width;
        previewBuffer = new byte[(height * width *3)/2]; //getPreviewFormat() then use the getBitsPerPixel(int) / 8

    }

    private Handler getHandler()
    {
        try
        {
                handlerInitLatch.await();
        }
        catch(InterruptedException e)
        {
            //find what to do here
        }
        return handler;
    }

    private final class DecodeHandler extends Handler
    {
        private final Map<DecodeHintType,Object> hints;
        private int decodeAttemptCount;

        DecodeHandler()
        {
            hints = new EnumMap<>(DecodeHintType.class);
            hints.put(DecodeHintType.POSSIBLE_FORMATS, Arrays.asList(BarcodeFormat.AZTEC, BarcodeFormat.QR_CODE, BarcodeFormat.DATA_MATRIX));
            hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
            decodeAttemptCount = 0;
        }

        @Override
        public void handleMessage(Message message)
        {
            if (!running)
            {
                return;
            }
            switch (message.what)
            {
                case R.id.decode_start:
                    camera.setPreviewCallbackWithBuffer(DecodeRunnable.this);
                    camera.addCallbackBuffer(previewBuffer);
                    break;
                case R.id.decode:
                    decode((byte[]) message.obj);
                    break;
                case R.id.decode_succeeded:
                    final Result result = (Result) message.obj;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            activity.setResult(result);
                        }
                    });
                    break;
                case R.id.decode_failed:
                    Arrays.fill(previewBuffer, (byte) 0x0);
                    if(++decodeAttemptCount < 500 )
                    {
                        Log.i(TAG, "Handle Message: decode failed. Attempt no. " + decodeAttemptCount);
                        camera.addCallbackBuffer(previewBuffer);
                    }
                    else
                    {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                activity.setResult(null);
                            }
                        });
                    }
                    break;
                case R.id.quit:
                    running = false;
                    Looper.myLooper().quit();
                    break;

            }
        }
        private void decode(byte[] data) {
            Result rawResult = null;

            int subtendedWidth = width / CameraConfigurationManager.ZOOM;
            int subtendedHeight = height / CameraConfigurationManager.ZOOM;
            int excessWidth = width - subtendedWidth;
            int excessHeight = height - subtendedHeight;

            //long start = System.currentTimeMillis();
            PlanarYUVLuminanceSource source =
                    new PlanarYUVLuminanceSource(data,
                            width, height,
                            excessWidth / 2, excessHeight / 2,
                            subtendedWidth, subtendedHeight,
                            false);
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
            try {
                rawResult = new MultiFormatReader().decode(bitmap, hints);
            } catch (ReaderException re) {
                Log.i(TAG, "decode exception" + re.getStackTrace());
            }

            //long end = System.currentTimeMillis();
            //Log.i(TAG, "Decode in " + (end - start));
            Handler handler = getHandler();
            if (rawResult == null) {
                handler.sendEmptyMessage(R.id.decode_failed); //obtainMessage more efficient than creating and allocating new message instances
            } else {
                Log.i(TAG, "Decode succeeded: " + rawResult.getText());
                handler.obtainMessage(R.id.decode_succeeded, rawResult).sendToTarget();
            };
        }

    }

    void startScanning()
    {
        getHandler().sendEmptyMessage(R.id.decode_start);
    }

    void stop()
    {
        getHandler().sendEmptyMessage(R.id.quit);
    }

    @Override
    public void onPreviewFrame(byte[] bytes, Camera camera)
    {
        if (running)
        {
            getHandler().obtainMessage(R.id.decode, bytes).sendToTarget();
        }
    }

    @Override
    public void run()
    {
        Looper.prepare();
        handler = new DecodeHandler();
        handlerInitLatch.countDown();
        Looper.loop();
    }
}*/

final class DecodeRunnable implements Runnable, IStillModeCallback {

    private static final String TAG = "DecodeRunnable";

    private final AbstractStillMode mode;
    //private final Camera camera;
    private byte[] previewBuffer;
    private boolean running;
    private Handler handler;
    private final CountDownLatch handlerInitLatch;

    DecodeRunnable(AbstractStillMode mode) {  //the decode logic applies only to still image
        this.mode = mode;
        //previewBuffer =  /*set proper size*/
        running = true;
        handlerInitLatch = new CountDownLatch(1);
    }

    private Handler getHandler() {
        try {
            handlerInitLatch.await();
        } catch (InterruptedException ie) {
            // continue?
        }
        return handler;
    }


    @Override
    public void run() {
        Looper.prepare();
        handler = new DecodeHandler();
        handlerInitLatch.countDown();
        Looper.loop();
    }

    void startScanning() {
        getHandler().obtainMessage(R.id.decode_start).sendToTarget();
    }

    void stop() {
        getHandler().obtainMessage(R.id.quit).sendToTarget();
    }

    @Override
    public void onStillModeFrame(byte[] data) {
        Log.d(TAG, "onStillModeFrame: will send image data");
        if (running) {
            getHandler().obtainMessage(R.id.decode, data).sendToTarget();
        }
    }

    private final class DecodeHandler extends Handler {

        private final Map<DecodeHintType,Object> hints;
        private int decodeAttemptCount;

        DecodeHandler() {
            hints = new EnumMap<>(DecodeHintType.class);
            hints.put(DecodeHintType.POSSIBLE_FORMATS,
                    Arrays.asList(BarcodeFormat.AZTEC, BarcodeFormat.QR_CODE, BarcodeFormat.DATA_MATRIX));
        }

        @Override
        public void handleMessage(Message message) {
            if (!running) {
                Log.i(TAG, "handleMessage: decode handler got running flase");
                return;
            }
            switch (message.what) {
                case R.id.decode_start:
                    mode.setCallback(DecodeRunnable.this);
                    break;
                case R.id.decode:
                    decode((byte[]) message.obj);
                    break;
                case R.id.decode_succeeded:
                    final Result result = (Result) message.obj;
                    mode.setResult(result);
                    break;
                case R.id.decode_failed:
                    Log.i(TAG, "Handle Message: decode failed. Attempt no. " + (++decodeAttemptCount));
                    mode.setResult(null);
                    break;
                case R.id.quit:
                    running = false;
                    Looper.myLooper().quit();
                    break;
            }
        }

        private void decode(byte[] data) {
            Result rawResult = null;

            Bitmap bMap = BitmapFactory.decodeByteArray(data, 0, data.length);
            int bMapWidth = bMap.getWidth();
            int bMapHeight = bMap.getHeight();
            int subtendedWidth = bMapWidth / mode.mZoom;
            int subtendedHeight = bMapHeight / mode.mZoom;
            int excessWidth = bMapWidth - subtendedWidth;
            int excessHeight = bMapHeight - subtendedHeight;
            /*because of Zoom*/
            bMap = Bitmap.createBitmap(bMap, excessWidth/2, excessHeight/2, subtendedWidth, subtendedHeight);
            bMapWidth = bMap.getWidth();
            bMapHeight = bMap.getHeight();
            Log.d(TAG, "decode: final bitmap sizes. W:"+bMapWidth+" H:"+bMapHeight);
            int[] pixels = new int[bMapWidth*bMapHeight];
            bMap.getPixels(pixels,0, bMapWidth, 0, 0, bMapWidth, bMapHeight);
            //long start = System.currentTimeMillis();
            LuminanceSource source = new RGBLuminanceSource(bMapWidth, bMapHeight, pixels);
            /*PlanarYUVLuminanceSource source =
                    new PlanarYUVLuminanceSource(data,
                            width, height,
                            excessWidth / 2, excessHeight / 2,
                            subtendedWidth, subtendedHeight,
                            false);*/
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
            try {
                rawResult = new MultiFormatReader().decode(bitmap, hints);
            } catch (ReaderException re) {
                Log.i(TAG, "decode exception" + re.getStackTrace());
            }

            //long end = System.currentTimeMillis();
            //Log.i(TAG, "Decode in " + (end - start));
            Handler handler = getHandler();
            Message message;
            if (rawResult == null) {
                message = handler.obtainMessage(R.id.decode_failed);
            } else {
                Log.i(TAG, "Decode succeeded: " + rawResult.getText());
                message = handler.obtainMessage(R.id.decode_succeeded, rawResult);
            }
            message.sendToTarget();
        }

    }

}
