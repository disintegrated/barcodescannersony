package de.tum.mw.fml.barcodescannersony;


/**
 * Created by RabbidDog on 12/21/2015.
 */
public interface IStillModeCallback {
    void onStillModeFrame(byte[] data);
}
