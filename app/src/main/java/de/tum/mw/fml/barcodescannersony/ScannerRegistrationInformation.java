package de.tum.mw.fml.barcodescannersony;

/**
 * Created by RabbidDog on 12/20/2015.
 */

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import com.sony.smarteyeglass.extension.util.SmartEyeglassWidgetImage;
import com.sonyericsson.extras.liveware.aef.registration.Registration.ExtensionColumns;
import com.sonyericsson.extras.liveware.extension.util.ExtensionUtils;
import com.sonyericsson.extras.liveware.extension.util.registration.RegistrationInformation;

public class ScannerRegistrationInformation extends RegistrationInformation
{

    /** The application context. */
    private final Context context;
    private final String TAG = "RegistrationInformation" ;
    /** Uses control API version*/
    private static final int CONTROL_API_VERSION = 4;
    private static final int WIDGET_API_VERSION = 3;

    protected ScannerRegistrationInformation(final Context context)
    {
        this.context = context;
    }

    @Override
    public int getRequiredNotificationApiVersion()
    {
        Log.d(TAG, "getRequiredNotificationApiVersion");
        return RegistrationInformation.API_NOT_REQUIRED;
    }

    @Override
    public int getRequiredWidgetApiVersion() {
        Log.d(TAG, "getRequiredWidgetApiVersion: ");
        return  RegistrationInformation.API_NOT_REQUIRED;
    }

    @Override
    public int getTargetWidgetApiVersion() {
        Log.d(TAG, "getTargetWidgetApiVersion: ");
        return  RegistrationInformation.API_NOT_REQUIRED;
    }

    @Override
    public int getRequiredControlApiVersion() {
        Log.d(TAG, "getRequiredControlApiVersion: ");
        return CONTROL_API_VERSION;
    }

    @Override
    public int getRequiredSensorApiVersion() {
        Log.d(TAG, "getRequiredSensorApiVersion: ");
        return RegistrationInformation.API_NOT_REQUIRED;
    }

    @Override
    public ContentValues getExtensionRegistrationConfiguration() {
        Log.d(TAG, "getExtensionRegistrationConfiguration: start");
        String iconHostapp =
                ExtensionUtils.getUriString(context, R.drawable.ic_glass_logo);
        String iconExtension =
                ExtensionUtils.getUriString(context, R.drawable.ic_glass_logo); //optional

        ContentValues values = new ContentValues();
        values.put(ExtensionColumns.CONFIGURATION_ACTIVITY,
                CameraPreferenceActivity.class.getName()); //optional
        values.put(ExtensionColumns.CONFIGURATION_TEXT,
                context.getString(R.string.configuration_text));//optional
        values.put(ExtensionColumns.NAME,
                context.getString(R.string.extension_name));
        values.put(ExtensionColumns.EXTENSION_KEY, Constants.EXTENSION_KEY);
        values.put(ExtensionColumns.HOST_APP_ICON_URI, iconHostapp);
        values.put(ExtensionColumns.EXTENSION_ICON_URI, iconExtension);
        values.put(ExtensionColumns.NOTIFICATION_API_VERSION,
                getRequiredNotificationApiVersion());
        values.put(ExtensionColumns.PACKAGE_NAME, context.getPackageName());
        Log.d(TAG, "getExtensionRegistrationConfiguration: end");
        return values;
    }

    @Override
    public boolean isDisplaySizeSupported(final int width, final int height) {
        ScreenSize size = new ScreenSize(context);
        return (size.equals(width, height));
    }

    /*@Override
    public boolean isWidgetSizeSupported(final int width, final int height) {
        return (height == SmartEyeglassWidgetImage.getSupportedWidgetHeight(context) &&
                width == SmartEyeglassWidgetImage.getSupportedWidgetWidth(context));
    }*/

}
